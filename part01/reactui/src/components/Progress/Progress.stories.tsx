import React, { useState } from 'react'
import Radio from './index'
import { withKnobs, text, boolean, select, number, color } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'
import Progress from './index'
import {Icon} from '../Icon'

export default {
    title: 'Progress',
    component: Progress,
    decorators: [withKnobs]
}
export const knobsProgress = () => (
    <Progress 
    count={number('count', 50, {range: true, min: 0, max: 100, step: 1})}
    countNumber={boolean('countNumber', true)}
    height={number('height', 8)}
    circle={boolean('circle', false)}
    size={number('size', 100)}
    primary={color('primary', '#FF4785')}
    secondary={color('secondary', '#FFAE00')}
    bottomColor={color('bottomColor', '#DDD')}
    flashColor={color('flashColor', '#FFF')}
    progressText={text('progressText', '')}
    />
)

export const circle= () => <Progress count={80} circle={true} />
export const progressText = () => (
    <Progress count={11} progressText={'yehuozhili'} />
)
export const changeColor = () => (
    <Progress
    count={20}
    primary='blue'
    secondary='yellow'
    bottomColor='brown'
     />
)
export const withIcon = () => (
    <Progress
    count={11}
    progressText={
        <span><Icon icon='admin' /></span>
    }
     />
)