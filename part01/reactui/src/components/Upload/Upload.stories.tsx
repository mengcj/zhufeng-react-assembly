import React from "react";
import { Upload } from "./index";
import {
	withKnobs,
	text,
	boolean,
	color,
	select,
	number,
} from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { Method, AxiosRequestConfig } from "axios";

export default {
	title: "Upload",
	component: Upload,
	decorators: [withKnobs],
};

// 只有单选模式才能裁剪
export const imgSingle = () => <Upload uploadMode="img" />;

export const imgMultiple = () => <Upload uploadMode="img" multiple={true} />;




export const defaultUpload = () => <Upload />;

export const progressUpload = () => <Upload progress={true} />;




const methods: Method[] = [
	"get",
	"GET",
	"delete",
	"DELETE",
	"head",
	"HEAD",
	"options",
	"OPTIONS",
	"post",
	"POST",
	"put",
	"PUT",
	"patch",
	"PATCH",
	"link",
	"LINK",
	"unlink",
	"UNLINK",
];

export const knobsUpload = () => {
	const uploadMode = select("uploadMode", ["default", "img"], "default");
	const axiosConfig: Partial<AxiosRequestConfig> = {
		url: text("url", "http://localhost:51111/user/uploadAvatar/"),
		method: select("method", methods, "post"),
	};
	const uploadFilename = text("uploadFilename", "avatar");

	return (
		<Upload
			multiple={boolean("multiple", false)}
			accept={text("accept", "*")}
			progress={boolean("progress", false)}
			max={number("max", 100)}
			onProgress={action("onProgress")}
			onRemoveCallback={action("onRemoveCallback")}
			uploadFilename={uploadFilename}
			axiosConfig={axiosConfig}
			uploadMode={uploadMode}
		></Upload>
	);
};