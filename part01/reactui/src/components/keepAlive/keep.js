import React, {createContext, useState, useEffect, useRef, useCallback, useMemo, Children, useContext} from 'react'
const Context = createContext()

export function AliveScope(props) {
    const [state, setState] = useState({})
    const ref = useMemo(() => {
        return {}
    }, [])
    const keep = useMemo(() => {
        return (id, Children) => (
            new Promise((resolve) => {
                setState({
                    [id]: {id, Children}
                })
                setTimeout(() => {
                    resolve(ref[id])
                })
            })
        )
    }, [ref])
    return (
        <Context.Provider value={keep}>
            {props.Children}
            {
                Object.values(state).map(({id, Children}) => (
                    <div key={id} ref={(node) => ref[id] = node}>
                        {Children}
                    </div>
                ))
            }
        </Context.Provider>
    )
}

function KeepAlive(props) {
    const keep = useContext(Context)
    useEffect(() => {
        const init = async ({id, Children}) => {
            const realContent = await keep(id, Children)
            if (ref.current) {
                ref.current.appendChild(realContent)
            }
        }
        init(props)
    }, [props, keep])
    const ref = useRef(null)
    return <div ref={ref} />
}

export default KeepAlive;
