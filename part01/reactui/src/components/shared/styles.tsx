import {css} from 'styled-components'
export const color = {
    primary: "#1890FF",
	secondary: "#13C2C2",
	tertiary: "#DDDDDD",
	orange: "#FA8C16",
	gold: "#FFAE00",
	green: "#52C41A",
	seafoam: "#37D5D3",
	purple: "#6F2CAC",
	ultraviolet: "#2A0481",
	// Monochrome
	lightest: "#FFFFFF",
	lighter: "#F8F8F8",
	light: "#F3F3F3",
	mediumlight: "#EEEEEE",
	medium: "#DDDDDD",
	mediumdark: "#999999",
	dark: "#666666",
	darker: "#444444",
	darkest: "#333333",
	border: "rgba(0,0,0,.1)",
	// Status
	positive: "#66BF3C",
	negative: "#FF4D4F",
	warning: "#FAAD14",
}

export const background = {
    app: "#F6F9FC",
	appInverse: "#7A8997",
	positive: "#E1FFD4",
	negative: "#FEDED2",
	warning: "#FFF5CF",
}

export const typography = {
	type: {
		primary:
			'"Nunito Sans", "Helvetica Neue", Helvetica, Arial, sans-serif',
		code://书写字体
			'"SFMono-Regular", Consolas, "Liberation Mono", Menlo, Courier, monospace',
	},
	weight: {
		regular: "400",
		bold: "700",
		extrabold: "800",
		black: "900",
	},
	size: {
		s1: "12",
		s2: "14",
		s3: "16",
		m1: "20",
		m2: "24",
		m3: "28",
		l1: "32",
		l2: "40",
		l3: "48",
	},
};

export const spacing = {
	padding: {
		small: 10,
		medium: 20,
		large: 30,
	},
	borderRadius: {
		small: 4,
		default: 8,
	},
};
export const breakpoint = 600;
export const pageMargin = 5;
export const btnPadding = {
	small: '8px 16px',
	medium: '13px 20px'
}

export const badgeColor = {
	positive: color.positive,
	negative: color.negative,
	neutral: color.dark,
	warning: color.warning,
	error: color.lightest
}

export const badgeBackground = {
	positive: background.positive,
	negative: background.negative,
	neutral: color.mediumlight,
	warning: background.warning,
	error: color.negative
}