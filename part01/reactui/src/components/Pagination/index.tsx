import React, { useState, useMemo, useEffect } from 'react'
import styled, { CSSProperties } from 'styled-components'
import { color } from '../shared/styles';
import { Icon } from '../Icon'
import Button from '../Button'
const columns = [
	{
		title: "Name",
		dataIndex: "name",
	},
	{
		title: "Chinese Score",
		dataIndex: "chinese",
	},
	{
		title: "Math Score",
		dataIndex: "math",
	},
	{
		title: "English Score",
		dataIndex: "english",
	},
];
const data = [
	{
		key: "1",
		name: "John Brown",
		chinese: 55,
		math: 60,
		english: 70,
	},
	{
		key: "2",
		name: "Jim Green",
		chinese: 98,
		math: 66,
		english: 89,
	},
	{
		key: "3",
		name: "Joe Black",
		chinese: 78,
		math: 90,
		english: 70,
	},
	{
		key: "4",
		name: "Jim Red",
		chinese: 88,
		math: 99,
		english: 89,
	},
];
const PageUl = styled.ul`
	display: flex;
	justify-content: center;
	align-items: center;

	& > li {
		list-style: none;
	}
	& button {
		border-radius: 6px;
		padding: 10px 8px;
	}
	& span {
		line-height: 13.6px;
		height: 13.6px;
		min-width: 18px;
	}
	& svg {
		height: 13.6px;
		width: 13.6px;
		vertical-align: bottom;
	}
`;

export type PaginationProps = {
    pageSize?: number;
    defaultCurrent?: number;
    total: number;
    barMaxSize?: number;
    callback?: (v: number) => void;
    style?: CSSProperties;
    classnames?: string;
}

export interface SourceDataType {
    key: string;
    [key: string]: any;
}

export interface columnType {
    title: ReactNode;
    dataIndex: string;
    sorter?: {
        compare: (a: SourceDataType, b: SourceDataType) => number;
    };
    render?: (v: any, value: SourceDataType, rowData: columnType) => ReactNode;
}

export type TableProps = {
    data: SourceDataType[];
    columns: columnType[];
    sorted?: boolean;
    pagination?: boolean;
    pageSize?: number;
}

function calculateMove(current: number, state: number[], totalPage: number): number[] | null {
    let mid = Math.floor(state.length / 2)
    let arr
    let minus = current - state[mid]
    if (minus === 0) {
        arr = null
    } else if (minus > 0) {
        let tmp = state[state.length - 1]
        if (tmp + minus < totalPage) {
            arr = state.map(v => v + minus)
        } else {
            if (tmp === totalPage) {
                arr = null
            } else {
                arr = state.map(v => v + totalPage - tmp)
            }
        }
    } else {
        if (state[0] + minus > 1) {
            arr = state.map(v => v + minus)
        } else {
            if (state[0] === 1) {
                arr = null
            } else {
                arr = state.map(v => v - state[0] + 1)
            }
        }
    }
    return arr
}

export function Pagination(props: PaginationProps) {
    const {callback} = props
    const pageSize  = 10
    const defaultCurrent = 1
    const barMaxSize = 5
    const total = 1000
    const [current, setCurrent] = useState(defaultCurrent)
    const [state, setState] = useState<Array<number>>([])
    const totalPage = useMemo(() => {
        let number = Math.ceil(total / pageSize)
        if (number > barMaxSize) {
            let statetmp = new Array(barMaxSize).fill(1).map((_x, y) => y + 1)
            setState(statetmp)
            let arr = calculateMove(defaultCurrent, statetmp, number)
            if (arr) {
                setState(arr)
            }
        } else {
            let statetmp = new Array(number).fill(1).map((_x, y) => y + 1)
            setState(statetmp)
            let arr = calculateMove(defaultCurrent, statetmp, number)
            if (arr) {
                setState(arr)
            }
        }
        return number
    }, [pageSize, total])

    useEffect(() => {
        callback && callback(current)
    }, [callback, current])

    return (
        <PageUl style={{ display: 'flex' }}>
            <li>
            <Button
					appearance="primaryOutline"
					disabled={current === 1 ? true : false}
					onClick={() => {
						if (state.length > 0) {
							if (state[0] > 1) {
								let statetmp = state.map((x) => x - 1);
								setState(statetmp);
								setCurrent(current - 1);
								let arr = calculateMove(
									current - 1,
									statetmp,
									totalPage
								);
								if (arr) {
									setState(arr);
								}
							} else if (current !== state[0]) {
								setCurrent(current - 1);
								let arr = calculateMove(
									current - 1,
									state,
									totalPage
								);
								if (arr) {
									setState(arr);
								}
							}
						}
					}}
				>
                    <Icon icon='arrowleft' color={color.primary} />
                </Button>
            </li>
            {
                state.map((x, i) => {
                    return <li key={i}>
                        <Button
                            appearance={
                                current === x ? 'primary' : 'primaryOutline'
                            }
                            onClick={() => {
                                setCurrent(x)
                                let arr = calculateMove(x, state, totalPage)
                                if (arr) {
                                    setState(arr)
                                }
                            }}
                        >
                            {x}
                        </Button>
                    </li>
                })
            }
            <li>
            <Button
					appearance="primaryOutline"
					disabled={current === totalPage ? true : false}
					onClick={() => {
						if (state.length > 0) {
							if (state[barMaxSize! - 1] < totalPage) {
								let statetmp = state.map((x) => x + 1);
								setState(statetmp);
								setCurrent(current + 1);
								let arr = calculateMove(
									current + 1,
									statetmp,
									totalPage
								);
								if (arr) {
									setState(arr);
								}
							} else {
								if (current !== totalPage) {
									setCurrent(current + 1);
									let arr = calculateMove(
										current + 1,
										state,
										totalPage
									);
									if (arr) {
										setState(arr);
									}
								}
							}
						}
					}}
				>
                    <Icon icon='arrowright' color={color.primary} />
                </Button>
            </li>
        </PageUl>
    )
}

Pagination.defaultProps = {
    pageSize: 10,
    defaultCurrent: 11,
    barMaxSize: 5,
    total: 1000
};